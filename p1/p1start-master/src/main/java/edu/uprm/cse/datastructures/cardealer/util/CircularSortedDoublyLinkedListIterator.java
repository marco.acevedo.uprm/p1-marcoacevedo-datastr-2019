package edu.uprm.cse.datastructures.cardealer.util;


import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
	private CircularSortedDoublyLinkedList<E> list;
	private int j;

	public CircularSortedDoublyLinkedListIterator(CircularSortedDoublyLinkedList<E> list) {
		this.list=list;
		this.j=0;
	}
	
	public CircularSortedDoublyLinkedListIterator(CircularSortedDoublyLinkedList<E> list, int index) {
		this.list=list;
		this.j=index;
	}
	
	@Override
	public boolean hasNext() {
		return j<this.list.size();
	}

	@Override
	public E next() {
		if (!hasNext()){
			throw new NoSuchElementException("No more elements to iterate over.");
		} 
		else{
			return list.get(j++);
		}
		
	}
	
	public void remove() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Remove peration not implemented.");
	}

}
