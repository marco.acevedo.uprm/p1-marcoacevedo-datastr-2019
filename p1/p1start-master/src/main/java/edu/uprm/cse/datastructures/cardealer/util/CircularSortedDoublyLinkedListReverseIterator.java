package edu.uprm.cse.datastructures.cardealer.util;

import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedListReverseIterator <E> implements ReverseIterator<E> {
	private CircularSortedDoublyLinkedList<E> list;
	private int j;

	public CircularSortedDoublyLinkedListReverseIterator(CircularSortedDoublyLinkedList<E> list) {//Default constructor
		this.list=list;
		this.j=list.size()-1;
	}
	
	public CircularSortedDoublyLinkedListReverseIterator(CircularSortedDoublyLinkedList<E> list,int index) {
		this.list=list;
		this.j=index;
	}
	
	@Override
	public boolean hasPrevious() {
		return j>=0;
	}

	@Override
	public E previous() {
		if (!hasPrevious()){
			throw new NoSuchElementException("No more elements to iterate over.");
		} 
		else{
			return list.get(j--);
		}
		
	}
	
	public void remove() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Remove peration not implemented.");
	}
}
