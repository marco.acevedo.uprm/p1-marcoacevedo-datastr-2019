package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.*;

public class CarList {
	private static SortedList<Car> carList = null;
	
		public CarList() {
			carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		}
		
		public static void resetCars() {
			carList=null;
			carList=new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		}

		public static SortedList<Car> getInstance() {
			if(carList==null) {
				carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
				return carList;
				
			}else {
				return carList;
			}
		}

}
