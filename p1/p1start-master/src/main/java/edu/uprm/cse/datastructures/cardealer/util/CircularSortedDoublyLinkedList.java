package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private int size;
	private Dnode<E> header;
	private Comparator<E> cmp;
	
	public CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		this.size=0;
		this.header = new Dnode<E>();
		this.header.setNext(header);
		this.header.setPrev(header);
		this.cmp=cmp;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>(this);
	}
	
	public Iterator<E> iterator(int index) {//Iterator starting from the given index
		this.checkIndex(index, "public Iterator<E> iterator(int index)");
		return new CircularSortedDoublyLinkedListIterator<E>(this, index);
	}
	
	public ReverseIterator<E> reverseIterator(){
		return new CircularSortedDoublyLinkedListReverseIterator<E>(this);
	}
	
	public ReverseIterator<E> reverseIterator(int index){//Reverse Iterator starting at the given index
		this.checkIndex(index, "public ReverseIterator<E> reverseIterator(int index)");
		return new CircularSortedDoublyLinkedListReverseIterator<E>(this, index);
	}

	@Override
	public boolean add(E obj) {//Adds new obj in the place that it should go to maintain sorted order
		Dnode<E> current = new Dnode<E>(obj);
		if(this.isEmpty()) {
			this.addBetween(header, current, header);
			size++;
			return true;
		}		
		Dnode<E> temp = this.header.getNext();	
		while(temp!=header && cmp.compare(obj, temp.getElement())>0) {
			temp=temp.getNext();
		}
		this.addBetween(temp.getPrev(), current, temp);
		size++;
		return true;	
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean remove(E obj) {
		if(this.isEmpty() || !this.contains(obj)) return false;
		
		this.remove(this.firstIndex(obj));
		return true;
	}

	@Override
	public boolean remove(int index) {
		if(this.isEmpty()) {
			throw new IndexOutOfBoundsException("Method: public boolean remove(int index); index is out of bounds.");
		}
		
			checkIndex(index,"public boolean remove(int index)");
			Dnode<E> temp = this.getNode(index);		
			temp.getPrev().setNext(temp.getNext());
			temp.getNext().setPrev(temp.getPrev());
			if(temp!=header) {
				temp.cleanLinks();
				temp=null;
			}
			size--;
			return true;
	}

	@Override
	public int removeAll(E obj) {
		int i=0;
		while(this.contains(obj)) {
			remove(obj);
			i++;
		}
		return i;
	}

	@Override
	public E first() {
		return (this.isEmpty() ? null : (E) this.header.getNext().getElement());
	}

	@Override
	public E last() {
		return (this.isEmpty() ? null : (E) this.header.getPrev().getElement());
	}

	@Override
	public E get(int index) {
		if(this.size()>0) {
			checkIndex(index,"public E get(int index)");
			return this.getNode(index).getElement();
		}		
		return null;
	}

	@Override
	public void clear() {
		while(this.header.getNext()!=header) {
			Dnode<E> temp = header.getNext();
			header.setNext(temp.getNext());
			temp.getNext().setPrev(header);
			temp.cleanLinks();
			temp=null;
		}
		this.size=0;
	}

	@Override
	public boolean contains(E e) {//must test
		if(this.isEmpty()) return false;
		boolean rtn = false;
		Dnode<E> temp = header.getNext();
		while(temp!=header && !rtn) {
			if(temp.getElement().equals(e)) {
				rtn=true;
			}
			temp=temp.getNext();
		}
		return rtn;
	}

	@Override
	public boolean isEmpty() {
		return this.size()==0;
	}

	@Override
	public int firstIndex(E e) {
		// TODO Auto-generated method stub
		if(this.isEmpty() || (!this.contains(e))) return -1;
		int i = 0;
		Dnode<E> temp = header.getNext();
		while(temp!=header) {
			if(temp.getElement().equals(e)) {
				return i;
			}
			temp=temp.getNext();
			i++;
		}
		return i;
	}

	@Override
	public int lastIndex(E e) {
		// TODO Auto-generated method stub
		if(this.isEmpty() || (!this.contains(e))) return -1;
		int i =this.size-1;
		Dnode<E> temp = header.getPrev();
		while(temp!=header) {
			if(temp.getElement().equals(e)) {
				return i;
			}
			temp=temp.getPrev();
			i--;
		}
		return i;
	}
	
	private Dnode<E> getNode(int index) {
		int i=0;
		Dnode<E> temp = header.getNext();
		while(i!=index) {
			temp=temp.getNext();
			i++;
		}
		return temp;
	}
	
	private void addBetween(Dnode<E> prev, Dnode<E> current, Dnode<E> next) {//Does NOT increase size!
		prev.setNext(current);
		next.setPrev(current);
		current.setNext(next);
		current.setPrev(prev);
	}
	
	private void checkIndex(int index, String str) {//index can't be negative or greater than size-1
		if(index<0 || index>this.size()-1) {
			throw new NodeOutOfBoundsException("Method: "+str+"; index is out of bounds.");
		}
	}
	
	
private static class Dnode<E>{
		private E element;
		private Dnode<E> next;
		private Dnode<E> prev;
		
		public Dnode() {
			this.element=null;
			this.next=null;
			this.prev=null;
		}
		
		public Dnode(E obj) {
			this.element=obj;
			this.next=null;
			this.prev=null;
		}
		
		public E getElement() {
			return element;
		}
		
		public void setElement(E element) {
			this.element = element;
		}
		
		public Dnode<E> getNext() {
			return next;
		}
		
		public Dnode<E> getPrev() {
			return prev;
		}
		
		public void setNext(Dnode<E> next) {
			this.next = next;
		}
		
		public void setPrev(Dnode<E> prev) {
			this.prev = prev;
		}
		
		public void cleanLinks() {
			this.element=null;
			this.next=null;
			this.prev=null;
		}

}
	
}
