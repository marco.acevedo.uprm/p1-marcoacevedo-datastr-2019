package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.core.MediaType;
import java.util.Optional;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;
import javax.ws.rs.core.Response;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {	
	private final SortedList<Car> dealer = CarList.getInstance();
	 
	/*method to read all cars as an array of cars
	 * 
	 * curl -X GET -i http://localhost:8080/cardealer/cars
	 * 
	 * http://localhost:8080/cardealer/cars/
	*/
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] readAsArray() {
		Car[] carArray = new Car[dealer.size()];
		for(int i=0;i<dealer.size();i++) {
			carArray[i]=dealer.get(i);
		}
		return carArray;
	}	
	
	/* Method to read a car with a given id
	 * 
	 * curl -X GET -i http://localhost:8080/cardealer/cars/id
	 * 
	 * http://localhost:8080/cardealer/cars/{id}
	 */
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getFromID(@PathParam("id") long id){
		if(dealer.isEmpty()) { throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));}
		for(int i=0;i<dealer.size();i++) {
			if(dealer.get(i).getCarId()==id) {
				return dealer.get(i);
			}
		}
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	}		
	
	/* Method to add a new car to the system
	 * 
	 * curl -X POST -i -H "Content-Type: application/json" -d '{"carId": id, "carBrand" : "Brand", "carModel" : "Model", "carModelOption" : "ModelOption", "carPrice" : price}' http://localhost:8080/cardealer/cars/add
	 * 
	 * http://localhost:8080/cardealer/add
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNew(Car newCar) {
		dealer.add(newCar);
		return Response.status(201).build();
	}		
	

	/* Method to update an existing car in the system
	 * 
	 * curl -X PUT -i -H "Content-Type: application/json" -d '{"carId": id, "carBrand" : "Brand", "carModel" : "Model","carModelOption" : "ModelOption", "carPrice" : price}' http://localhost:8080/cardealer/cars/id/update
	 * 
	 * http://localhost:8080/cardealer/cars/{id}/update
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") long id, Car c) {
		if(dealer.isEmpty()) return Response.status(Response.Status.NOT_FOUND).build();
		
		int i=0;
		while(dealer.get(i).getCarId()!=id) {
			i++;
		}
		if(dealer.get(i).getCarId()!=id) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}else {
			dealer.remove(i);
			dealer.add(c);
			return Response.status(Response.Status.OK).build();
		}
	}	
	
	/* Method to delete an existing car from the system
	 * 
	 * curl -X DELETE -i http://localhost:8080/cardealer/cars/id/delete
	 * 
	 * http://localhost:8080/cardealer/cars/{id}/delete
	 */
	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") long id) {
		int i = 0;
		while(dealer.get(i).getCarId()!=id && i<dealer.size()) {
			i++;
		}
		if(dealer.get(i).getCarId()==id){
			dealer.remove(i);
			return Response.status(200).build();
		}
		return Response.status(404).build();
	}
}
