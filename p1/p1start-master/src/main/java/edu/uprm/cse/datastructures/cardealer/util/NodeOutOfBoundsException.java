package edu.uprm.cse.datastructures.cardealer.util;

public class NodeOutOfBoundsException extends RuntimeException{
	public NodeOutOfBoundsException() {
		super("An unidentified error occured");
	}

	public NodeOutOfBoundsException(String arg0) {
		super(arg0);
	}

	public NodeOutOfBoundsException(Throwable arg0) {
		super(arg0);
	}

	public NodeOutOfBoundsException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
